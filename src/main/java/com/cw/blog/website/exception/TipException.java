package com.cw.blog.website.exception;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/4 10:08
 */
public class TipException extends RuntimeException{
    public TipException() {
    }

    public TipException(String message) {
        super(message);
    }

    public TipException(String message, Throwable cause) {
        super(message, cause);
    }

    public TipException(Throwable cause) {
        super(cause);
    }
}
