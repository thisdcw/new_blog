package com.cw.blog.website.controller.admin;

import com.cw.blog.website.controller.BaseController;
import com.cw.blog.website.dto.LogActions;
import com.cw.blog.website.dto.Types;
import com.cw.blog.website.exception.TipException;
import com.cw.blog.website.modal.Content;
import com.cw.blog.website.modal.ContentInstance;
import com.cw.blog.website.modal.Meta;
import com.cw.blog.website.modal.User;
import com.cw.blog.website.modal.bo.RestResponseBo;
import com.cw.blog.website.service.IContentService;
import com.cw.blog.website.service.ILogService;
import com.cw.blog.website.service.IMetaService;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/10 11:21
 */
@Controller
@RequestMapping("/admin/article")
@Transactional(rollbackFor = TipException.class)
public class ArticleController extends BaseController {
    
        private static final Logger LOGGER = LoggerFactory.getLogger(ArticleController.class);

        @Resource
        private IContentService contentsService;

        @Resource
        private IMetaService metasService;

        @Resource
        private ILogService logService;

        /**
         * 文章列表
         * @param page
         * @param limit
         * @param request
         * @return
         */
        @GetMapping(value = "")
        public String index(@RequestParam(value = "page", defaultValue = "1") int page,
                            @RequestParam(value = "limit", defaultValue = "15") int limit, HttpServletRequest request) {
            ContentInstance contentVoInstance = new ContentInstance();
            contentVoInstance.setOrderByClause("created desc");
            contentVoInstance.createCriteria().andTypeEqualTo(Types.ARTICLE.getType());
            PageInfo<Content> contentsPaginator = contentsService.getArticlesWithpage(contentVoInstance,page,limit);
            request.setAttribute("articles", contentsPaginator);
            return "admin/article_list";
        }

        /**
         * 文章发表
         * @param request
         * @return
         */
        @GetMapping(value = "/publish")
        public String newArticle(HttpServletRequest request) {
            List<Meta> categories = metasService.getMetas(Types.CATEGORY.getType());
            request.setAttribute("categories", categories);
            return "admin/article_edit";
        }

        /**
         * 文章编辑
         * @param cid
         * @param request
         * @return
         */
        @GetMapping(value = "/{cid}")
        public String editArticle(@PathVariable String cid, HttpServletRequest request) {
            Content contents = contentsService.getContents(cid);
            request.setAttribute("contents", contents);
            List<Meta> categories = metasService.getMetas(Types.CATEGORY.getType());
            request.setAttribute("categories", categories);
            request.setAttribute("active", "article");
            return "admin/article_edit";
        }

        /**
         * 文章发表
         * @param contents
         * @param request
         * @return
         */
        @PostMapping(value = "/publish")
        @ResponseBody
        @Transactional(rollbackFor = TipException.class)
        public RestResponseBo publishArticle(Content contents, HttpServletRequest request) {
            User users = this.user(request);
            contents.setAuthor_id(users.getUid());
            contents.setType(Types.ARTICLE.getType());
            if (StringUtils.isBlank(contents.getCategories())) {
                contents.setCategories("默认分类");
            }
            try {
                contentsService.publish(contents);
            } catch (Exception e) {
                String msg = "文章发布失败";
                if (e instanceof TipException) {
                    msg = e.getMessage();
                } else {
                    LOGGER.error(msg, e);
                }
                return RestResponseBo.fail(msg);
            }
            return RestResponseBo.ok();
        }

        /**
         * 文章更新
         * @param contents
         * @param request
         * @return
         */
        @PostMapping(value = "/modify")
        @ResponseBody
        @Transactional(rollbackFor = TipException.class)
        public RestResponseBo modifyArticle(Content contents,HttpServletRequest request) {
            User users = this.user(request);
            contents.setAuthor_id(users.getUid());
            contents.setType(Types.ARTICLE.getType());
            try {
                contentsService.updateArticle(contents);
            } catch (Exception e) {
                String msg = "文章编辑失败";
                if (e instanceof TipException) {
                    msg = e.getMessage();
                } else {
                    LOGGER.error(msg, e);
                }
                return RestResponseBo.fail(msg);
            }
            return RestResponseBo.ok();
        }

        /**
         * 删除文章
         * @param cid
         * @param request
         * @return
         */
        @RequestMapping(value = "/delete")
        @ResponseBody
        @Transactional(rollbackFor = TipException.class)
        public RestResponseBo delete(@RequestParam int cid, HttpServletRequest request) {
            try {
                contentsService.deleteByCid(cid);
                logService.insertLog(LogActions.DEL_ARTICLE.getAction(), cid+"", request.getRemoteAddr(), this.getUid(request));
            } catch (Exception e) {
                String msg = "文章删除失败";
                if (e instanceof TipException) {
                    msg = e.getMessage();
                } else {
                    LOGGER.error(msg, e);
                }
                return RestResponseBo.fail(msg);
            }
            return RestResponseBo.ok();
        }
}
