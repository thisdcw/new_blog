package com.cw.blog.website.controller;

import com.cw.blog.website.exception.TipException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/10 10:23
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    public static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(value = TipException.class)
    public String tipException(Exception e){
        LOGGER.error("find exception:e{}",e.getMessage());
        e.printStackTrace();
        return "comm/error_500";
    }
}
