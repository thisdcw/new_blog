package com.cw.blog.website.controller.admin;

import com.cw.blog.website.constant.WebConst;
import com.cw.blog.website.controller.BaseController;
import com.cw.blog.website.dto.LogActions;
import com.cw.blog.website.dto.Types;
import com.cw.blog.website.exception.TipException;
import com.cw.blog.website.modal.Content;
import com.cw.blog.website.modal.ContentInstance;
import com.cw.blog.website.modal.User;
import com.cw.blog.website.modal.bo.RestResponseBo;
import com.cw.blog.website.service.IContentService;
import com.cw.blog.website.service.ILogService;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/10 11:36
 */
@Controller()
@RequestMapping("admin/page")
public class PageController extends BaseController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PageController.class);

    @Resource
    private IContentService contentsService;

    @Resource
    private ILogService logService;

    @GetMapping(value = "")
    public String index(HttpServletRequest request) {
        ContentInstance contentVoInstance = new ContentInstance();
        contentVoInstance.setOrderByClause("created desc");
        contentVoInstance.createCriteria().andTypeEqualTo(Types.PAGE.getType());
        PageInfo<Content> contentsPaginator = contentsService.getArticlesWithpage(contentVoInstance, 1, WebConst.MAX_POSTS);
        request.setAttribute("articles", contentsPaginator);
        return "admin/page_list";
    }

    @GetMapping(value = "new")
    public String newPage(HttpServletRequest request) {
        return "admin/page_edit";
    }

    @GetMapping(value = "/{cid}")
    public String editPage(@PathVariable String cid, HttpServletRequest request) {
        Content contents = contentsService.getContents(cid);
        request.setAttribute("contents", contents);
        return "admin/page_edit";
    }

    @PostMapping(value = "publish")
    @ResponseBody
    @Transactional(rollbackFor = TipException.class)
    public RestResponseBo publishPage(@RequestParam String title, @RequestParam String content,
                                      @RequestParam String status, @RequestParam String slug,
                                      @RequestParam(required = false) Integer allow_comment,
                                      @RequestParam(required = false) Integer allow_ping, HttpServletRequest request) {

        User users = this.user(request);
        Content contents = new Content();
        contents.setTitle(title);
        contents.setContent(content);
        contents.setStatus(status);
        contents.setSlug(slug);
        contents.setType(Types.PAGE.getType());
        if (null != allow_comment) {
            contents.setAllow_comment(allow_comment == 1);
        }
        if (null != allow_ping) {
            contents.setAllow_ping(allow_ping == 1);
        }
        contents.setAuthor_id(users.getUid());

        try {
            contentsService.publish(contents);
        } catch (Exception e) {
            String msg = "页面发布失败";
            if (e instanceof TipException) {
                msg = e.getMessage();
            } else {
                LOGGER.error(msg, e);
            }
            return RestResponseBo.fail(msg);
        }
        return RestResponseBo.ok();
    }

    @PostMapping(value = "modify")
    @ResponseBody
    @Transactional(rollbackFor = TipException.class)
    public RestResponseBo modifyArticle(@RequestParam Integer cid, @RequestParam String title,
                                        @RequestParam String content,
                                        @RequestParam String status, @RequestParam String slug,
                                        @RequestParam(required = false) Integer allow_comment,
                                        @RequestParam(required = false) Integer allow_ping,
                                        HttpServletRequest request) {

        User users = this.user(request);
        Content contents = new Content();
        contents.setCid(cid);
        contents.setTitle(title);
        contents.setContent(content);
        contents.setStatus(status);
        contents.setSlug(slug);
        contents.setType(Types.PAGE.getType());
        if (null != allow_comment) {
            contents.setAllow_comment(allow_comment == 1);
        }
        if (null != allow_ping) {
            contents.setAllow_ping(allow_ping == 1);
        }
        contents.setAuthor_id(users.getUid());
        try {
            contentsService.updateArticle(contents);
        } catch (Exception e) {
            String msg = "页面编辑失败";
            if (e instanceof TipException) {
                msg = e.getMessage();
            } else {
                LOGGER.error(msg, e);
            }
            return RestResponseBo.fail(msg);
        }
        return RestResponseBo.ok();
    }

    @RequestMapping(value = "delete")
    @ResponseBody
    @Transactional(rollbackFor = TipException.class)
    public RestResponseBo delete(@RequestParam int cid, HttpServletRequest request) {
        try {
            contentsService.deleteByCid(cid);
            logService.insertLog(LogActions.DEL_PAGE.getAction(), cid + "", request.getRemoteAddr(), this.getUid(request));
        } catch (Exception e) {
            String msg = "页面删除失败";
            if (e instanceof TipException) {
                msg = e.getMessage();
            } else {
                LOGGER.error(msg, e);
            }
            return RestResponseBo.fail(msg);
        }
        return RestResponseBo.ok();
    }
}