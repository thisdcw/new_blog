package com.cw.blog.website.controller;

import com.cw.blog.website.modal.User;
import com.cw.blog.website.utils.TaleUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/4 13:06
 */
public abstract class BaseController {
    public static String THEME = "themes/default";

    /**
     * 主页的页面主题
     * @param viewName
     * @return
     */
    public String render(String viewName) {
        return THEME + "/" + viewName;
    }

    public BaseController title(HttpServletRequest request, String title) {
        request.setAttribute("title", title);
        return this;
    }

    public BaseController keywords(HttpServletRequest request, String keywords) {
        request.setAttribute("keywords", keywords);
        return this;
    }

    /**
     * 获取请求绑定的登录对象
     * @param request
     * @return
     */
    public User user(HttpServletRequest request) {
        return TaleUtils.getLoginUser(request);
    }

    public Integer getUid(HttpServletRequest request){
        return this.user(request).getUid();
    }

    public String render_404() {
        return "comm/error_404";
    }
}
