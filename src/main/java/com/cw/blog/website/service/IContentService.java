package com.cw.blog.website.service;

import com.cw.blog.website.modal.Content;
import com.cw.blog.website.modal.ContentInstance;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Repository;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/4 11:40
 */
public interface IContentService {

    /**
     * 发布文章
     * @param contents
     */
    void publish(Content contents);

    /**
     *查询文章返回多条数据
     * @param p 当前页
     * @param limit 每页条数
     * @return ContentVo
     */
    PageInfo<Content> getContents(Integer p, Integer limit);


    /**
     * 根据id或slug获取文章
     *
     * @param id id
     * @return ContentVo
     */
    Content getContents(String id);

    /**
     * 根据主键更新
     * @param contentVo contentVo
     */
    void updateContentByCid(Content contentVo);


    /**
     * 查询分类/标签下的文章归档
     * @param mid mid
     * @param page page
     * @param limit limit
     * @return ContentVo
     */
    PageInfo<Content> getArticles(Integer mid, int page, int limit);

    /**
     * 搜索、分页
     * @param keyword keyword
     * @param page page
     * @param limit limit
     * @return ContentVo
     */
    PageInfo<Content> getArticles(String keyword,Integer page,Integer limit);


    /**
     * @param contentInstance
     * @param page
     * @param limit
     * @return
     */
    PageInfo<Content> getArticlesWithpage(ContentInstance contentInstance, Integer page, Integer limit);
    /**
     * 根据文章id删除
     * @param cid
     */
    void deleteByCid(Integer cid);

    /**
     * 编辑文章
     * @param contents
     */
    void updateArticle(Content contents);


    /**
     * 更新原有文章的category
     * @param ordinal
     * @param newCatefory
     */
    void updateCategory(String ordinal,String newCatefory);
}
