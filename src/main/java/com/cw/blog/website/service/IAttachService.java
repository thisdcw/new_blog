package com.cw.blog.website.service;

import com.cw.blog.website.modal.Attach;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/3 13:05
 */
public interface IAttachService {
    PageInfo<Attach> getAttachs(Integer page, Integer limit);

    void save(String fname, String fkey, String ftype, Integer author);

    Attach selectById(Integer id);

    void deleteById(Integer id);
}
