package com.cw.blog.website.service;

import com.cw.blog.website.dto.MetaDto;
import com.cw.blog.website.modal.Comment;
import com.cw.blog.website.modal.Content;
import com.cw.blog.website.modal.bo.ArchiveBo;
import com.cw.blog.website.modal.bo.BackResponseBo;
import com.cw.blog.website.modal.bo.StatisticsBo;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/4 11:01
 */
public interface ISiteService {

    /**
     * 最新收到的评论
     *
     * @param limit
     * @return
     */
    List<Comment> recentComments(int limit);

    /**
     * 最新发表的文章
     *
     * @param limit
     * @return
     */
    List<Content> recentContents(int limit);

    /**
     * 查询一条评论
     * @param coid
     * @return
     */
    Comment getComment(Integer coid);

    /**
     * 系统备份
     * @param bk_type
     * @param bk_path
     * @param fmt
     * @return
     */
    BackResponseBo backup(String bk_type, String bk_path, String fmt) throws Exception;


    /**
     * 获取后台统计数据
     *
     * @return
     */
    StatisticsBo getStatistics();

    /**
     * 查询文章归档
     *
     * @return
     */
    List<ArchiveBo> getArchives();

    /**
     * 获取分类/标签列表
     * @return
     */
    List<MetaDto> metas(String type, String orderBy, int limit);

}
