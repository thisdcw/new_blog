package com.cw.blog.website.service;

import com.cw.blog.website.modal.Log;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/4 11:42
 */
public interface ILogService {

    /**
     * 保存操作日志
     *
     * @param logVo
     */
    void insertLog(Log logVo);

    /**
     *  保存
     * @param action
     * @param data
     * @param ip
     * @param author_id
     */
    void insertLog(String action, String data, String ip, Integer author_id);

    /**
     * 获取日志分页
     * @param page 当前页
     * @param limit 每页条数
     * @return 日志
     */
    List<Log> getLogs(int page, int limit);
}
