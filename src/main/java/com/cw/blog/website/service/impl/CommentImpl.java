package com.cw.blog.website.service.impl;

import com.cw.blog.website.dao.CommentMapper;
import com.cw.blog.website.exception.TipException;
import com.cw.blog.website.modal.Comment;
import com.cw.blog.website.modal.CommentInstance;
import com.cw.blog.website.modal.Content;
import com.cw.blog.website.modal.bo.CommentBo;
import com.cw.blog.website.service.ICommentService;
import com.cw.blog.website.service.IContentService;
import com.cw.blog.website.utils.DateKit;
import com.cw.blog.website.utils.TaleUtils;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/4 10:07
 */
@Service
public class CommentImpl implements ICommentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommentImpl.class);

    @Resource
    private CommentMapper commentMapper;

    @Resource
    @Lazy
    private IContentService contentService;

    @Override
    public void insertComment(Comment comments) {
        if (null == comments){
            throw new TipException("评论对象为空");
        }
        if (StringUtils.isBlank(comments.getAuthor())) {
            comments.setAuthor("热心网友");
        }
        if (StringUtils.isNotBlank(comments.getMail()) && !TaleUtils.isEmail(comments.getMail())) {
            throw new TipException("请输入正确的邮箱格式");
        }
        if (StringUtils.isBlank(comments.getContent())) {
            throw new TipException("评论内容不能为空");
        }
        if (comments.getContent().length() < 5 || comments.getContent().length() > 2000) {
            throw new TipException("评论字数在5-2000个字符");
        }
        if (null == comments.getCid()) {
            throw new TipException("评论文章不能为空");
        }
        Content contents = contentService.getContents(String.valueOf(comments.getCid()));
        if (null == contents) {
            throw new TipException("不存在的文章");
        }
        comments.setOwner_id(contents.getAuthor_id());
        comments.setCreated(DateKit.getCurrentUnixTime());
        commentMapper.insertSelective(comments);

        Content temp = new Content();
        temp.setCid(contents.getCid());
        temp.setComments_num(contents.getComments_num() + 1);
        contentService.updateContentByCid(temp);
    }

    @Override
    public PageInfo<CommentBo> getComments(Integer cid, int page, int limit) {
        return null;
    }

    @Override
    public PageInfo<Comment> getCommentsWithPage(CommentInstance commentInstance, int page, int limit) {
        return null;
    }

    @Override
    public Comment getCommentById(Integer coid) {
        return null;
    }

    @Override
    public void delete(Integer coid, Integer cid) {

    }

    @Override
    public void update(Comment comment) {

    }
}
