package com.cw.blog.website.service.impl;

import com.cw.blog.website.dao.OptionMapper;
import com.cw.blog.website.modal.Option;
import com.cw.blog.website.modal.OptionInstance;
import com.cw.blog.website.service.IOptionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/4 12:28
 */
@Service
public class OptionImpl implements IOptionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(OptionImpl.class);

    @Resource
    private OptionMapper optionDao;

    @Override
    public void insertOption(Option optionVo) {
        LOGGER.debug("Enter insertOption method:optionVo={}" ,optionVo);
        optionDao.insertSelective(optionVo);
        LOGGER.debug("Exit insertOption method.");
    }

    @Override
    public void insertOption(String name, String value) {
        LOGGER.debug("Enter insertOption method:name={},value={}",name,value );
        Option optionVo = new Option();
        optionVo.setName(name);
        optionVo.setValue(value);
        if(optionDao.selectByInstance(new OptionInstance()).size()==0){
            optionDao.insertSelective(optionVo);
        }else{
            optionDao.updateByPrimaryKeySelective(optionVo);
        }
        LOGGER.debug("Exit insertOption method.");
    }

    @Override
    public void saveOptions(Map<String, String> options) {
        if (null != options && !options.isEmpty()) {
            options.forEach(this::insertOption);
        }
    }

    @Override
    public List<Option> getOptions(){
        return optionDao.selectByInstance(new OptionInstance());
    }
}
