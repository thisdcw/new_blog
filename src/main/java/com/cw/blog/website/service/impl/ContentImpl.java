package com.cw.blog.website.service.impl;

import com.cw.blog.website.constant.WebConst;
import com.cw.blog.website.dao.ContentMapper;
import com.cw.blog.website.dao.MetaMapper;
import com.cw.blog.website.dto.Types;
import com.cw.blog.website.exception.TipException;
import com.cw.blog.website.modal.Content;
import com.cw.blog.website.modal.ContentInstance;
import com.cw.blog.website.service.IContentService;
import com.cw.blog.website.service.IMetaService;
import com.cw.blog.website.service.IRelationshipService;
import com.cw.blog.website.utils.DateKit;
import com.cw.blog.website.utils.TaleUtils;
import com.cw.blog.website.utils.Tools;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.vdurmont.emoji.EmojiParser;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/4 12:10
 */
@Service
public class ContentImpl implements IContentService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ContentImpl.class);

    @Resource
    private ContentMapper contentDao;

    @Resource
    @Lazy
    private MetaMapper metaDao;

    @Resource
    @Lazy
    private IRelationshipService relationshipService;

    @Resource
    @Lazy
    private IMetaService metasService;

    @Override
    public void publish(Content contents) {
        if (null == contents) {
            throw new TipException("文章对象为空");
        }
        if (StringUtils.isBlank(contents.getTitle())) {
            throw new TipException("文章标题不能为空");
        }
        if (StringUtils.isBlank(contents.getContent())) {
            throw new TipException("文章内容不能为空");
        }
        int titleLength = contents.getTitle().length();
        if (titleLength > WebConst.MAX_TITLE_COUNT) {
            throw new TipException("文章标题过长");
        }
        int contentLength = contents.getContent().length();
        if (contentLength > WebConst.MAX_TEXT_COUNT) {
            throw new TipException("文章内容过长");
        }
        if (null == contents.getAuthor_id()) {
            throw new TipException("请登录后发布文章");
        }
        if (StringUtils.isNotBlank(contents.getSlug())) {
            if (contents.getSlug().length() < 5) {
                throw new TipException("路径太短了");
            }
            if (!TaleUtils.isPath(contents.getSlug())) throw new TipException("您输入的路径不合法");
            ContentInstance contentInstance = new ContentInstance();
            contentInstance.createCriteria().andTypeEqualTo(contents.getType()).andStatusEqualTo(contents.getSlug());
            long count = contentDao.countByInstance(contentInstance);
            if (count > 0) throw new TipException("该路径已经存在，请重新输入");
        } else {
            contents.setSlug(null);
        }

        contents.setContent(EmojiParser.parseToAliases(contents.getContent()));

        int time = DateKit.getCurrentUnixTime();
        contents.setCreated(time);
        contents.setModified(time);
        contents.setHits(0);
        contents.setComments_num(0);

        String tags = contents.getTags();
        String categories = contents.getCategories();
        contentDao.insert(contents);
        Integer cid = contents.getCid();

        metasService.saveMetas(cid, tags, Types.TAG.getType());
        metasService.saveMetas(cid, categories, Types.CATEGORY.getType());
    }

    @Override
    public PageInfo<Content> getContents(Integer p, Integer limit) {
        LOGGER.debug("Enter getContents method");
        ContentInstance contentInstance = new ContentInstance();
        contentInstance.setOrderByClause("created desc");
        contentInstance.createCriteria().andTypeEqualTo(Types.ARTICLE.getType()).andStatusEqualTo(Types.PUBLISH.getType());
        PageHelper.startPage(p, limit);
        List<Content> data = contentDao.selectByInstanceWithBLOBs(contentInstance);
        PageInfo<Content> pageInfo = new PageInfo<>(data);
        LOGGER.debug("Exit getContents method");
        return pageInfo;
    }

    @Override
    public Content getContents(String id) {
        if (StringUtils.isNotBlank(id)) {
            if (Tools.isNumber(id)) {
                Content contentVo = contentDao.selectByPrimaryKey(Integer.valueOf(id));
                if (contentVo != null) {
                    contentVo.setHits(contentVo.getHits() + 1);
                    contentDao.updateByPrimaryKey(contentVo);
                }
                return contentVo;
            } else {
                ContentInstance contentInstance = new ContentInstance();
                contentInstance.createCriteria().andSlugEqualTo(id);
                List<Content> contentVos = contentDao.selectByInstanceWithBLOBs(contentInstance);
                if (contentVos.size() != 1) {
                    throw new TipException("query content by id and return is not one");
                }
                return contentVos.get(0);
            }
        }
        return null;
    }

    @Override
    public void updateContentByCid(Content contentVo) {
        if (null != contentVo && null != contentVo.getCid()) {
            contentDao.updateByPrimaryKeySelective(contentVo);
        }
    }

    @Override
    public PageInfo<Content> getArticles(Integer mid, int page, int limit) {
        int total = metaDao.countWithSql(mid);
        PageHelper.startPage(page, limit);
        List<Content> list = contentDao.findByCatalog(mid);
        PageInfo<Content> paginator = new PageInfo<>(list);
        paginator.setTotal(total);
        return paginator;
    }

    @Override
    public PageInfo<Content> getArticles(String keyword, Integer page, Integer limit) {
        PageHelper.startPage(page, limit);
        ContentInstance contentVoInstance = new ContentInstance();
        ContentInstance.Criteria criteria = contentVoInstance.createCriteria();
        criteria.andTypeEqualTo(Types.ARTICLE.getType());
        criteria.andStatusEqualTo(Types.PUBLISH.getType());
        criteria.andTitleLike("%" + keyword + "%");
        contentVoInstance.setOrderByClause("created desc");
        List<Content> contentVos = contentDao.selectByInstanceWithBLOBs(contentVoInstance);
        return new PageInfo<>(contentVos);
    }

    @Override
    public PageInfo<Content> getArticlesWithpage(ContentInstance commentVoInstance, Integer page, Integer limit) {
        PageHelper.startPage(page, limit);
        List<Content> contentVos = contentDao.selectByInstanceWithBLOBs(commentVoInstance);
        return new PageInfo<>(contentVos);
    }

    @Override
    public void deleteByCid(Integer cid) {
        Content contents = this.getContents(cid + "");
        if (null != contents) {
            contentDao.deleteByPrimaryKey(cid);
            relationshipService.deleteById(cid, null);
        }
    }

    @Override
    public void updateCategory(String ordinal, String newCatefory) {
        Content contentVo = new Content();
        contentVo.setCategories(newCatefory);
        ContentInstance Instance = new ContentInstance();
        Instance.createCriteria().andCategoriesEqualTo(ordinal);
        contentDao.updateByInstanceSelective(contentVo, Instance);
    }

    @Override
    public void updateArticle(Content contents) {
        if (null == contents || null == contents.getCid()) {
            throw new TipException("文章对象不能为空");
        }
        if (StringUtils.isBlank(contents.getTitle())) {
            throw new TipException("文章标题不能为空");
        }
        if (StringUtils.isBlank(contents.getContent())) {
            throw new TipException("文章内容不能为空");
        }
        if (contents.getTitle().length() > 200) {
            throw new TipException("文章标题过长");
        }
        if (contents.getContent().length() > 65000) {
            throw new TipException("文章内容过长");
        }
        if (null == contents.getAuthor_id()) {
            throw new TipException("请登录后发布文章");
        }
        if (StringUtils.isBlank(contents.getSlug())) {
            contents.setSlug(null);
        }
        int time = DateKit.getCurrentUnixTime();
        contents.setModified(time);
        Integer cid = contents.getCid();
        contents.setContent(EmojiParser.parseToAliases(contents.getContent()));

        contentDao.updateByPrimaryKeySelective(contents);
        relationshipService.deleteById(cid, null);
        metasService.saveMetas(cid, contents.getTags(), Types.TAG.getType());
        metasService.saveMetas(cid, contents.getCategories(), Types.CATEGORY.getType());
    }
}
