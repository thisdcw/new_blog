package com.cw.blog.website.service;

import com.cw.blog.website.modal.Relationship;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/4 12:02
 */
public interface IRelationshipService {

    /**
     * 按住键删除
     * @param cid
     * @param mid
     */
    void deleteById(Integer cid, Integer mid);

    /**
     * 按主键统计条数
     * @param cid
     * @param mid
     * @return 条数
     */
    Long countById(Integer cid, Integer mid);


    /**
     * 保存對象
     * @param relationship
     */
    void insertVo(Relationship relationship);

    /**
     * 根据id搜索
     * @param cid
     * @param mid
     * @return
     */
    List<Relationship> getRelationshipById(Integer cid, Integer mid);
}
