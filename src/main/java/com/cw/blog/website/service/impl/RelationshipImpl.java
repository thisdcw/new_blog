package com.cw.blog.website.service.impl;

import com.cw.blog.website.dao.RelationshipMapper;
import com.cw.blog.website.modal.Relationship;
import com.cw.blog.website.modal.RelationshipInstance;
import com.cw.blog.website.service.IRelationshipService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/4 12:30
 */
@Service
public class RelationshipImpl implements IRelationshipService {
    private static final Logger LOGGER = LoggerFactory.getLogger(RelationshipImpl.class);

    @Resource
    private RelationshipMapper relationshipVoMapper;

    @Override
    public void deleteById(Integer cid, Integer mid) {
        RelationshipInstance relationshipInstance = new RelationshipInstance();
        RelationshipInstance.Criteria criteria = relationshipInstance.createCriteria();
        if (cid != null) {
            criteria.andCidEqualTo(cid);
        }
        if (mid != null) {
            criteria.andMidEqualTo(mid);
        }
        relationshipVoMapper.deleteByInstance(relationshipInstance);
    }

    @Override
    public List<Relationship> getRelationshipById(Integer cid, Integer mid) {
        RelationshipInstance relationshipInstance = new RelationshipInstance();
        RelationshipInstance.Criteria criteria = relationshipInstance.createCriteria();
        if (cid != null) {
            criteria.andCidEqualTo(cid);
        }
        if (mid != null) {
            criteria.andMidEqualTo(mid);
        }
        return relationshipVoMapper.selectByInstance(relationshipInstance);
    }

    @Override
    public void insertVo(Relationship relationshipVoKey) {
        relationshipVoMapper.insert(relationshipVoKey);
    }

    @Override
    public Long countById(Integer cid, Integer mid) {
        LOGGER.debug("Enter countById method:cid={},mid={}",cid,mid);
        RelationshipInstance relationshipInstance = new RelationshipInstance();
        RelationshipInstance.Criteria criteria = relationshipInstance.createCriteria();
        if (cid != null) {
            criteria.andCidEqualTo(cid);
        }
        if (mid != null) {
            criteria.andMidEqualTo(mid);
        }
        long num = relationshipVoMapper.countByInstance(relationshipInstance);
        LOGGER.debug("Exit countById method return num={}",num);
        return num;
    }
}
