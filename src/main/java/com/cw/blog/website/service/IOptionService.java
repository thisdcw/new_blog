package com.cw.blog.website.service;

import com.cw.blog.website.modal.Option;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/4 12:01
 */
public interface IOptionService {

    void insertOption(Option optionVo);

    void insertOption(String name, String value);

    List<Option> getOptions();


    /**
     * 保存一组配置
     *
     * @param options
     */
    void saveOptions(Map<String, String> options);
}
