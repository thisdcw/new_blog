package com.cw.blog.website.service.impl;

import com.cw.blog.website.dao.UserMapper;
import com.cw.blog.website.exception.TipException;
import com.cw.blog.website.modal.User;
import com.cw.blog.website.modal.UserInstance;
import com.cw.blog.website.service.IUserService;
import com.cw.blog.website.utils.TaleUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/4 12:35
 */
@Service
public class UserImpl implements IUserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserImpl.class);

    @Resource
    private UserMapper userDao;

    @Override
    public Integer insertUser(User userVo) {
        Integer uid = null;
        if (StringUtils.isNotBlank(userVo.getUsername()) && StringUtils.isNotBlank(userVo.getEmail())) {
//            用户密码加密
            String encodePwd = TaleUtils.MD5encode(userVo.getUsername() + userVo.getPassword());
            userVo.setPassword(encodePwd);
            userDao.insertSelective(userVo);
        }
        return userVo.getUid();
    }

    @Override
    public User queryUserById(Integer uid) {
        User userVo = null;
        if (uid != null) {
            userVo = userDao.selectByPrimaryKey(uid);
        }
        return userVo;
    }

    @Override
    public User login(String username, String password) {
        if (StringUtils.isBlank(username) || StringUtils.isBlank(password)) {
            throw new TipException("用户名和密码不能为空");
        }
        UserInstance userInstance = new UserInstance();
        UserInstance.Criteria criteria = userInstance.createCriteria();
        criteria.andUsernameEqualTo(username);
        long count = userDao.countByInstance(userInstance);
        if (count < 1) {
            throw new TipException("不存在该用户");
        }
        String pwd = TaleUtils.MD5encode(username+password);
        criteria.andPasswordEqualTo(pwd);
        List<User> userVos = userDao.selectByInstance(userInstance);
        if (userVos.size()!=1) {
            throw new TipException("用户名或密码错误");
        }
        return userVos.get(0);
    }

    @Override
    public void updateByUid(User userVo) {
        if (null == userVo || null == userVo.getUid()) {
            throw new TipException("userVo is null");
        }
        int i = userDao.updateByPrimaryKeySelective(userVo);
        if(i!=1){
            throw new TipException("update user by uid and return is not one");
        }
    }
}
