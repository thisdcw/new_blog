package com.cw.blog.website.service;

import com.cw.blog.website.modal.Comment;
import com.cw.blog.website.modal.CommentInstance;
import com.cw.blog.website.modal.bo.CommentBo;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Repository;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/4 9:51
 */

public interface ICommentService {
    /**
     * 保存对象
     * @param comment
     * */
    void insertComment(Comment comment);

    /**
     * 获取文章下的评论
     * @param limit
     * @param page
     * @param cid
     * @returns CommentBo
     * */
    PageInfo<CommentBo> getComments(Integer cid, int page, int limit);

    /**
     * 获取文章下的评论
     * @param commentInstance
     * @param page
     * @param limit
     * @returns Comment
     * */
    PageInfo<Comment> getCommentsWithPage(CommentInstance commentInstance, int page, int limit);

    /**
     * 根据主键查询评论
     * @param coid
     * @returns Comment
    * */
    Comment getCommentById(Integer coid);

    /**
     * 删除评论,暂不用
     * @param cid
     * @param coid
    * */
    void delete(Integer coid, Integer cid);

    /**
     * 更新评论状态
     * @param comment
     * */
    void update(Comment comment);
}
