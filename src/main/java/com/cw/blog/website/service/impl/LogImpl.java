package com.cw.blog.website.service.impl;

import com.cw.blog.website.constant.WebConst;
import com.cw.blog.website.dao.LogMapper;
import com.cw.blog.website.modal.Log;
import com.cw.blog.website.modal.LogInstance;
import com.cw.blog.website.service.ILogService;
import com.cw.blog.website.utils.DateKit;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/4 12:20
 */
@Service
public class LogImpl implements ILogService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LogImpl.class);

    @Resource
    private LogMapper logDao;

    @Override
    public void insertLog(Log logVo) {
        logDao.insert(logVo);
    }

    @Override
    public void insertLog(String action, String data, String ip, Integer author_id) {
        Log logs = new Log();
        logs.setAction(action);
        logs.setData(data);
        logs.setIp(ip);
        logs.setAuthor_id(author_id);
        logs.setCreated(DateKit.getCurrentUnixTime());
        logDao.insert(logs);
    }

    @Override
    public List<Log> getLogs(int page, int limit) {
        LOGGER.debug("Enter getLogs method:page={},linit={}",page,limit);
        if (page <= 0) {
            page = 1;
        }
        if (limit < 1 || limit > WebConst.MAX_POSTS) {
            limit = 10;
        }
        LogInstance logInstance = new LogInstance();
        logInstance.setOrderByClause("id desc");
        PageHelper.startPage((page - 1) * limit, limit);
        List<Log> logVos = logDao.selectByInstance(logInstance);
        LOGGER.debug("Exit getLogs method");
        return logVos;
    }
}
