package com.cw.blog.website.service.impl;

import com.cw.blog.website.dao.AttachMapper;
import com.cw.blog.website.modal.Attach;
import com.cw.blog.website.modal.AttachInstance;
import com.cw.blog.website.service.IAttachService;
import com.cw.blog.website.utils.DateKit;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/3 13:06
 */
@Service
public class AttachImpl implements IAttachService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AttachImpl.class);

    @Autowired
    private AttachMapper attachMapper;

    @Override
    public PageInfo<Attach> getAttachs(Integer page, Integer limit) {
        PageHelper.startPage(page, limit);
        AttachInstance attachInstance = new AttachInstance();
        attachInstance.setOrderByCause("id decs");
        List<Attach> attaches = attachMapper.selectByInstance(attachInstance);
        return new PageInfo<>(attaches);
    }

    @Override
    public void save(String fname, String fkey, String ftype, Integer author) {
        Attach attach = new Attach();
        attach.setFname(fname);
        attach.setFkey(fkey);
        attach.setFtype(ftype);
        attach.setAuthor_id(author);
        attach.setCreated(DateKit.getCurrentUnixTime());
        attachMapper.insertSelective(attach);
    }

    @Override
    public Attach selectById(Integer id) {
        if (null != id) {
            return attachMapper.selectByPrimaryKey(id);
        }
        return null;
    }

    @Override
    public void deleteById(Integer id) {
        if (null != id) {
            attachMapper.deleteByPrimaryKey(id);
        }
    }
}
