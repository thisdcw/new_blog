package com.cw.blog.website.utils.backup;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/4 12:54
 */
@Data
@AllArgsConstructor
public class FK {
    private String column;
    private Table referenceTable;
    private String referencePK;

    public Table getReferenceTable() {
        return referenceTable;
    }
}
