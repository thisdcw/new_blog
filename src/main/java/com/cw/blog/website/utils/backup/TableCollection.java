package com.cw.blog.website.utils.backup;

import java.util.ArrayList;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/4 12:52
 */
public class TableCollection extends ArrayList<Table> {


    /**
     * Sort tables according to constraints
     * 该排序算法的目的是解决数据库中表之间存在外键约束（即一个表的某个字段引用了另一个表的主键）时可能出现的循环引用问题。
     * 通过将有循环引用的表放到集合的末尾，可以保证在备份或其他操作中先处理没有依赖的表，避免出现错误或异常情况
     */
    public void sort(){
        for(int i = 0 ; i < size(); ){
            boolean corrupted = false;
            for(int j = i + 1; j < size(); j++){
                if(get(i).isReferenced(get(j))){
                    Table table = get(i);
                    remove(table);
                    add(table);
                    corrupted = true;
                    break;
                }
            }
            if(!corrupted){
                i++;
            }
        }
    }
}
