package com.cw.blog.website.utils;

import com.google.gson.Gson;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/10 11:32
 */
public class GsonUtils {

    private static final Gson gson = new Gson();

    public static String toJsonString(Object object){
        return object==null?null:gson.toJson(object);
    }
}
