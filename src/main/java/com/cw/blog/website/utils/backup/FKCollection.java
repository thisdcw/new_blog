package com.cw.blog.website.utils.backup;

import java.util.ArrayList;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/4 12:55
 */
public class FKCollection extends ArrayList<FK> {
    public boolean isReferenced(Table referenceTable){
        for(FK fk : this){
            if(fk.getReferenceTable().equals(referenceTable)){
                return true;
            }
        }
        return false;
    }
}
