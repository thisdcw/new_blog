package com.cw.blog.website.utils.backup;

import lombok.ToString;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/4 12:52
 */
@ToString
public class Column {
    private String name;
    private String typeName;
    private int dataType;

    public String getName() {
        return name;
    }

    public int getDataType() {
        return dataType;
    }

    public Column(String name, String typeName, int dataType) {
        super();
        this.name = name;
        this.typeName = typeName;
        this.dataType = dataType;
    }
}
