package com.cw.blog.website.dao;

import com.cw.blog.website.dto.MetaDto;
import com.cw.blog.website.modal.Meta;
import com.cw.blog.website.modal.MetaInstance;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/4 11:30
 */
@Repository
@Mapper
public interface MetaMapper {
    long countByInstance(MetaInstance metaInstance);

    int deleteByInstance(MetaInstance metaInstance);

    int deleteByPrimaryKey(Integer mid);

    int insert(Meta record);

    int insertSelective(Meta record);

    List<Meta> selectByInstance(MetaInstance metaInstance);

    Meta selectByPrimaryKey(Integer mid);

    int updateByInstanceSelective(@Param("record") Meta record, @Param("instance") MetaInstance metaInstance);

    int updateByInstance(@Param("record") Meta record, @Param("instance") MetaInstance metaInstance);

    int updateByPrimaryKeySelective(Meta record);

    int updateByPrimaryKey(Meta record);

    List<MetaDto> selectFromSql(Map<String,Object> paraMap);

    MetaDto selectDtoByNameAndType(@Param("name") String name,@Param("type") String type);

    Integer countWithSql(Integer mid);
}
