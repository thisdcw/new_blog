package com.cw.blog.website.dao;

import com.cw.blog.website.modal.Relationship;
import com.cw.blog.website.modal.RelationshipInstance;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/4 11:34
 */
@Repository
@Mapper
public interface RelationshipMapper {
    long countByInstance(RelationshipInstance relationshipInstance);

    int deleteByInstance(RelationshipInstance relationshipInstance);

    int deleteByPrimaryKey(Relationship key);

    int insert(Relationship record);

    int insertSelective(Relationship record);

    List<Relationship> selectByInstance(RelationshipInstance relationshipInstance);

    int updateByInstanceSelective(@Param("record") Relationship record, @Param("instance") RelationshipInstance relationshipInstance);

    int updateByInstance(@Param("record") Relationship record, @Param("instance") RelationshipInstance relationshipInstance);

}
