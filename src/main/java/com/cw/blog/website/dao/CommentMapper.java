package com.cw.blog.website.dao;

import com.cw.blog.website.modal.Comment;
import com.cw.blog.website.modal.CommentInstance;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/4 9:39
 */
@Repository
@Mapper
public interface CommentMapper {
    long countByInstance(CommentInstance commentsInstance);

    int deleteByInstance(CommentInstance commentsInstance);

    int deleteByPrimaryKey(Integer coid);

    int insert(Comment record);

    int insertSelective(Comment record);

    List<Comment> selectByInstanceWithBlobs(CommentInstance commentInstance);

    List<Comment> selectByInstance(CommentInstance commentInstance);

    Comment selectByPrimaryKey(Integer coid);

    int updateByInstanceSelective(@Param("record") Comment record, @Param("instance") CommentInstance commentInstance);

    int updateByInstanceWithBlobs(@Param("record") Comment record, @Param("instance") CommentInstance commentInstance);

    int updateByInstance(@Param("record") Comment record, @Param("instance") CommentInstance commentInstance);

    int updateByPrimaryKeySelective(Comment record);

    int updateByPrimaryKeyWithBlobs(Comment record);

    int updateByPrimaryKey(Comment record);
}
