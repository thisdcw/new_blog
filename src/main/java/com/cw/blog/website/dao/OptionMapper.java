package com.cw.blog.website.dao;

import com.cw.blog.website.modal.Option;
import com.cw.blog.website.modal.OptionInstance;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/4 11:32
 */
@Repository
@Mapper
public interface OptionMapper {
    long countByInstance(OptionInstance optionInstance);

    int deleteByInstance(OptionInstance optionInstance);

    int deleteByPrimaryKey(String name);

    int insert(Option record);

    int insertSelective(Option record);

    List<Option> selectByInstance(OptionInstance optionInstance);

    Option selectByPrimaryKey(String name);

    int updateByInstanceSelective(@Param("record") Option record, @Param("instance") OptionInstance optionInstance);

    int updateByInstance(@Param("record") Option record, @Param("instance") OptionInstance optionInstance);

    int updateByPrimaryKeySelective(Option record);

    int updateByPrimaryKey(Option record);

    /**
     * 批量保存
     * @param optionVos list
     * @return 保存的个数
     */
    int insertOptions(List<Option> optionVos);
}
