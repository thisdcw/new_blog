package com.cw.blog.website.dao;

import com.cw.blog.website.modal.Log;
import com.cw.blog.website.modal.LogInstance;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/4 11:28
 */
@Repository
@Mapper
public interface LogMapper {
    long countByInstance(LogInstance logInstance);

    int deleteByInstance(LogInstance logInstance);

    int deleteByPrimaryKey(Integer id);

    int insert(Log record);

    int insertSelective(Log record);

    List<Log> selectByInstance(LogInstance logInstance);

    Log selectByPrimaryKey(Integer id);

    int updateByInstanceSelective(@Param("record") Log record, @Param("instance") LogInstance logInstance);

    int updateByInstance(@Param("record") Log record, @Param("instance") LogInstance logInstance);

    int updateByPrimaryKeySelective(Log record);

    int updateByPrimaryKey(Log record);
}
