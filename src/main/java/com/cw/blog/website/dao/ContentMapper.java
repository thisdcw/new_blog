package com.cw.blog.website.dao;

import com.cw.blog.website.modal.Content;
import com.cw.blog.website.modal.ContentInstance;
import com.cw.blog.website.modal.bo.ArchiveBo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/4 11:09
 */
@Repository
@Mapper
public interface ContentMapper {
    long countByInstance(ContentInstance contentInstance);

    int deleteByInstance(ContentInstance contentInstance);

    int deleteByPrimaryKey(Integer cid);

    int insert(Content record);

    int insertSelective(Content record);

    List<Content> selectByInstanceWithBLOBs(ContentInstance contentInstance);

    List<Content> selectByInstance(ContentInstance contentInstance);

    Content selectByPrimaryKey(Integer cid);

    int updateByInstanceSelective(@Param("record") Content record, @Param("Instance") ContentInstance contentInstance);

    int updateByInstanceWithBLOBs(@Param("record") Content record, @Param("Instance") ContentInstance contentInstance);

    int updateByInstance(@Param("record") Content record, @Param("Instance") ContentInstance contentInstance);

    int updateByPrimaryKeySelective(Content record);

    int updateByPrimaryKeyWithBLOBs(Content record);

    int updateByPrimaryKey(Content record);

    List<ArchiveBo> findReturnArchiveBo();

    List<Content> findByCatalog(Integer mid);
}
