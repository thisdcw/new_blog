package com.cw.blog.website.dao;

import com.cw.blog.website.modal.Attach;
import com.cw.blog.website.modal.AttachInstance;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/3 13:01
 */
@Repository
@Mapper
public interface AttachMapper {
    long countByInstance(AttachInstance attachInstance);

    int deleteByInstance(AttachInstance attachInstance);

    int deleteByPrimaryKey(Integer id);

    int insert(Attach record);

    int insertSelective(Attach record);

    List<Attach> selectByInstance(AttachInstance attachInstance);

    Attach selectByPrimaryKey(Integer id);

    int updateByInstanceSelective(@Param("record") Attach attach, @Param("instance") AttachInstance attachInstance);

    int updateByInstance(@Param("record") Attach attach, @Param("instance") AttachInstance attachInstance);

    int updateByPrimaryKeySelective(Attach record);

    int updateByPrimaryKey(Attach record);
}
