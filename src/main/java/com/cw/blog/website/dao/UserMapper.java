package com.cw.blog.website.dao;

import com.cw.blog.website.modal.User;
import com.cw.blog.website.modal.UserInstance;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/4 11:36
 */
@Repository
@Mapper
public interface UserMapper {
    long countByInstance(UserInstance userInstance);

    int deleteByInstance(UserInstance userInstance);

    int deleteByPrimaryKey(Integer uid);

    int insert(User record);

    int insertSelective(User record);

    List<User> selectByInstance(UserInstance userInstance);

    User selectByPrimaryKey(Integer uid);

    int updateByInstanceSelective(@Param("record") User record, @Param("instance") UserInstance userInstance);

    int updateByInstance(@Param("record") User record, @Param("instance") UserInstance userInstance);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);
}
