package com.cw.blog.website.modal;

import lombok.Data;

import java.io.Serializable;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/3 11:22
 */
@Data
public class Content implements Serializable {
    private Integer cid;
    private String title;
    private String slug;
    private Integer created;
    private Integer modified;
    private String content;
    private Integer author_id;
    private String type;
    private String status;
    private String tags;
    private String categories;
    private Integer hits;
    private Integer comments_num;
    private Boolean allow_comment;
    private Boolean allow_ping;
    private Boolean allow_feed;
}
