package com.cw.blog.website.modal.bo;

import com.cw.blog.website.modal.Content;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/4 11:05
 */
@Data
public class ArchiveBo implements Serializable {
    private String date;
    private String count;
    private List<Content> articles;
}
