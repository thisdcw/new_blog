package com.cw.blog.website.modal.bo;

import com.cw.blog.website.modal.Comment;

import java.util.List;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/4 9:55
 * 返回页面的评论,包含父子评论内容
 */
public class CommentBo extends Comment {
    private int levels;
    private List<Comment> children;

    public CommentBo(Comment comments) {
        setAuthor(comments.getAuthor());
        setMail(comments.getMail());
        setCoid(comments.getCoid());
        setAuthor_id(comments.getAuthor_id());
        setUrl(comments.getUrl());
        setCreated(comments.getCreated());
        setAgent(comments.getAgent());
        setIp(comments.getIp());
        setContent(comments.getContent());
        setOwner_id(comments.getOwner_id());
        setCid(comments.getCid());
    }

    public int getLevels() {
        return levels;
    }

    public void setLevels(int levels) {
        this.levels = levels;
    }

    public List<Comment> getChildren() {
        return children;
    }

    public void setChildren(List<Comment> children) {
        this.children = children;
    }
}
