package com.cw.blog.website.modal;

import lombok.Data;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/3 11:23
 */
@Data
public class Option {
    private String name;
    private String value;
    private String description;
}
