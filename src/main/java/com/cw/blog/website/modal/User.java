package com.cw.blog.website.modal;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/3 11:23
 */
@Data
public class User implements Serializable {
    private Integer uid;
    private String username;
    private String password;
    private String email;
    private String home_url;
    private String screen_name;
    private Integer created;
    private Integer activated;
    private Integer logged;
    private String group_name;
}
