package com.cw.blog.website.modal;

import lombok.Data;

import java.io.Serializable;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/3 11:23
 */
@Data
public class Relationship implements Serializable {
    private Integer cid;
    private Integer mid;
}
