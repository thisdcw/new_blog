package com.cw.blog.website.modal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/3 11:22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Attach implements Serializable {
    private Integer id;
    private String fname;
    private String ftype;
    private String fkey;
    private Integer author_id;
    private Integer created;
}
