package com.cw.blog.website.modal.bo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/4 11:04
 */
@Getter
@Setter
public class BackResponseBo implements Serializable {

    private String attachPath;
    private String themePath;
    private String sqlPath;
}
