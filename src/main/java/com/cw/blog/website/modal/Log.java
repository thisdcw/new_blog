package com.cw.blog.website.modal;

import lombok.Data;

import java.io.Serializable;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/3 11:22
 */
@Data
public class Log implements Serializable {
    private Integer id;
    private String action;
    private String data;
    private Integer author_id;
    private String ip;
    private Integer created;
}
