package com.cw.blog.website.modal;

import java.util.ArrayList;
import java.util.List;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/3 13:57
 */
public class AttachInstance {

    protected String orderByCause;
    protected boolean distinct;
    protected List<Criteria> oredCriteria;
    private Integer limit;
    private Integer offset;

    public AttachInstance(){
        oredCriteria = new ArrayList<>();
    }

    public String getOrderByCause() {
        return orderByCause;
    }

    public void setOrderByCause(String orderByCause) {
        this.orderByCause = orderByCause;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void setOredCriteria(List<Criteria> oredCriteria) {
        this.oredCriteria = oredCriteria;
    }
    protected Criteria createCriteriaInternal() {
        return new Criteria();
    }
    public void or(Criteria criteria){
        oredCriteria.add(criteria);
    }
    public Criteria or(){
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }
    public void clear() {
        oredCriteria.clear();
        orderByCause = null;
        distinct = false;
    }
    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public static class Criterion {
        private String condition;
        private Object value;
        private Object secondValue;
        private boolean noValue;
        private boolean singleValue;
        private boolean betweenValue;
        private boolean listValue;
        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if ((value instanceof List<?>)) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value, Object value2, String property) {
            if (value == null || value2 == null) {
                throw new RuntimeException("between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value, value2));
        }

        /**
         * Integer id部分
         */
        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id !=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id>=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id<", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id<=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        /**
         * String fname部分
         */
        public Criteria andFnameIsNull() {
            addCriterion("fname is null");
            return (Criteria) this;
        }

        public Criteria andFnameIsNotNull() {
            addCriterion("fname is not null");
            return (Criteria) this;
        }

        public Criteria andFnameEqualTo(String value) {
            addCriterion("fname =", value, "fname");
            return (Criteria) this;
        }

        public Criteria andFnameNotEqualTo(String value) {
            addCriterion("fname !=", value, "fname");
            return (Criteria) this;
        }

        public Criteria andFnameLike(String value) {
            addCriterion("fname like", value, "fname");
            return (Criteria) this;
        }

        public Criteria andFnameNotLike(String value) {
            addCriterion("fname not like", value, "fname");
            return (Criteria) this;
        }

        public Criteria andFnameIn(List<String> value) {
            addCriterion("fname in", value, "fname");
            return (Criteria) this;
        }

        public Criteria andFnameNotIn(List<String> value) {
            addCriterion("fname not in", value, "fname");
            return (Criteria) this;
        }

        public Criteria andFnameBetween(String value1, String value2) {
            addCriterion("fname between", value1, value2, "fname");
            return (Criteria) this;
        }

        public Criteria andFnameNotBetween(String value1, String value2) {
            addCriterion("fname not between", value1, value2, "fname");
            return (Criteria) this;
        }

        /**
         * String ftype部分
         */
        public Criteria andFtypeIsNull() {
            addCriterion("ftype is null");
            return (Criteria) this;
        }

        public Criteria andFtypeIsNotNull() {
            addCriterion("ftype is not null");
            return (Criteria) this;
        }

        public Criteria andFtypeEqualsTo(String value) {
            addCriterion("ftype =", value, "ftype");
            return (Criteria) this;
        }

        public Criteria andFtypeNotEqualsTo(String value) {
            addCriterion("ftype !=", value, "ftype");
            return (Criteria) this;
        }

        public Criteria andFtypeLike(String value) {
            addCriterion("ftype like", value, "ftype");
            return (Criteria) this;
        }

        public Criteria andFtypeNotLike(String value) {
            addCriterion("ftype not like", value, "ftype");
            return (Criteria) this;
        }

        public Criteria andFtypeIn(List<String> value) {
            addCriterion("ftype in", value, "ftype");
            return (Criteria) this;
        }

        public Criteria andFtypeNotIn(List<String> value) {
            addCriterion("ftype not in", value, "ftype");
            return (Criteria) this;
        }

        public Criteria andFtypeBetween(String value1, String value2) {
            addCriterion("ftype between", value1, value2, "ftype");
            return (Criteria) this;
        }

        public Criteria andFtypeNotBetween(String value1, String value2) {
            addCriterion("ftype not between", value1, value2, "ftype");
            return (Criteria) this;
        }

        /**
         * String fkey部分
         * */
        public Criteria andFkeyIsNull() {
            addCriterion("fkey is null");
            return (Criteria) this;
        }

        public Criteria andFkeyIsNotNull() {
            addCriterion("fkey is not null");
            return (Criteria) this;
        }
        public Criteria andFkeyEqualsTo(String value) {
            addCriterion("fkey =", value, "ftype");
            return (Criteria) this;
        }

        public Criteria andFkeyNotEqualsTo(String value) {
            addCriterion("fkey !=", value, "ftype");
            return (Criteria) this;
        }

        public Criteria andFkeyLike(String value) {
            addCriterion("fkey like", value, "ftype");
            return (Criteria) this;
        }

        public Criteria andFkeyNotLike(String value) {
            addCriterion("fkey not like", value, "ftype");
            return (Criteria) this;
        }

        public Criteria andFkeyIn(List<String> value) {
            addCriterion("fkey in", value, "ftype");
            return (Criteria) this;
        }

        public Criteria andFkeyNotIn(List<String> value) {
            addCriterion("fkey not in", value, "ftype");
            return (Criteria) this;
        }

        public Criteria andFkeyBetween(String value1, String value2) {
            addCriterion("fkey between", value1, value2, "ftype");
            return (Criteria) this;
        }

        public Criteria andFkeyNotBetween(String value1, String value2) {
            addCriterion("fkey not between", value1, value2, "ftype");
            return (Criteria) this;
        }

        /**
         * String author_id部分
         * */
        public Criteria andAuthorIdIsNull() {
            addCriterion("author_id is null");
            return (Criteria) this;
        }

        public Criteria andAuthorIdIsNotNull() {
            addCriterion("author_id is not null");
            return (Criteria) this;
        }
        public Criteria andAuthorIdEqualsTo(String value) {
            addCriterion("author_id =", value, "ftype");
            return (Criteria) this;
        }

        public Criteria andAuthorIdNotEqualsTo(String value) {
            addCriterion("author_id !=", value, "ftype");
            return (Criteria) this;
        }

        public Criteria andAuthorIdLike(String value) {
            addCriterion("author_id like", value, "ftype");
            return (Criteria) this;
        }

        public Criteria andAuthorIdNotLike(String value) {
            addCriterion("author_id not like", value, "ftype");
            return (Criteria) this;
        }

        public Criteria andAuthorIdIn(List<String> value) {
            addCriterion("author_id in", value, "ftype");
            return (Criteria) this;
        }

        public Criteria andAuthorIdNotIn(List<String> value) {
            addCriterion("author_id not in", value, "ftype");
            return (Criteria) this;
        }

        public Criteria andAuthorIdBetween(String value1, String value2) {
            addCriterion("author_id between", value1, value2, "ftype");
            return (Criteria) this;
        }

        public Criteria andAuthorIdNotBetween(String value1, String value2) {
            addCriterion("author_id not between", value1, value2, "ftype");
            return (Criteria) this;
        }

        /**
         * Integer created 部分
         * */
        public Criteria andCreatedIsNull() {
            addCriterion("created is null");
            return (Criteria) this;
        }

        public Criteria andCreatedIsNotNull() {
            addCriterion("created is not null");
            return (Criteria) this;
        }
        public Criteria andCreatedEqualsTo(Integer value) {
            addCriterion("created =", value, "ftype");
            return (Criteria) this;
        }

        public Criteria andCreatedNotEqualsTo(Integer value) {
            addCriterion("created !=", value, "ftype");
            return (Criteria) this;
        }

        public Criteria andCreatedGreaterThan(Integer value) {
            addCriterion("created >", value, "ftype");
            return (Criteria) this;
        }

        public Criteria andCreatedGreaterThanOrEqual(Integer value) {
            addCriterion("created >=", value, "ftype");
            return (Criteria) this;
        }

        public Criteria andCreatedLessThan(Integer value) {
            addCriterion("created <", value, "ftype");
            return (Criteria) this;
        }

        public Criteria andCreatedLessThanOrEqual(Integer value) {
            addCriterion("created <=", value, "ftype");
            return (Criteria) this;
        }

        public Criteria andCreatedIn(List<Integer> value) {
            addCriterion("created in", value, "ftype");
            return (Criteria) this;
        }

        public Criteria andCreatedNotIn(List<Integer> value) {
            addCriterion("created not in", value, "ftype");
            return (Criteria) this;
        }

        public Criteria andCreatedBetween(Integer value1, Integer value2) {
            addCriterion("created between", value1, value2, "ftype");
            return (Criteria) this;
        }

        public Criteria andCreatedNotBetween(Integer value1, Integer value2) {
            addCriterion("created not between", value1, value2, "ftype");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }
}
