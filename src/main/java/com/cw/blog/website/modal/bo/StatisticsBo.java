package com.cw.blog.website.modal.bo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/4 11:03
 */
@Data
public class StatisticsBo implements Serializable {

    private Long articles;
    private Long comments;
    private Long links;
    private Long attachs;
}
