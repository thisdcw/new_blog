package com.cw.blog.website.modal;

import lombok.Data;

import java.io.Serializable;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/3 11:22
 */
@Data
public class Meta implements Serializable {
    private Integer mid;
    private String name;
    private String slug;
    private String type;
    private String description;
    private Integer sort;
    private Integer parent;
}
