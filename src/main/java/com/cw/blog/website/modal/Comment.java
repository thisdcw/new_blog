package com.cw.blog.website.modal;

import lombok.Data;

import java.io.Serializable;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/3 11:22
 */
@Data
public class Comment implements Serializable {
    private Integer coid;
    private Integer cid;
    private Integer created;
    private String author;
    private Integer author_id;
    private Integer owner_id;
    private String mail;
    private String url;
    private String ip;
    private String agent;
    private String content;
    private String type;
    private String status;
    private Integer parent;
}
