package com.cw.blog.website.dto;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/10 11:09
 */
public interface ErrorCode {
    String BAD_REQUEST = "BAD REQUEST";
}
