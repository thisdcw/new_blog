package com.cw.blog.website.dto;

import com.cw.blog.website.modal.Meta;
import lombok.Getter;
import lombok.Setter;

/**
 * @author thisdcw@hotmail.com
 * @date 2023/7/4 10:53
 */
@Getter
@Setter
public class MetaDto extends Meta {
    private int count;
}
